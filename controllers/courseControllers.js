const User = require("../models/User.js");
const Course = require("../models/Course.js");
const auth = require("../auth.js");

// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (course) => {

	let newCourse = new Course({
		name: course.name,
		description: course.description,
		price: course.price
	});

	return newCourse.save().then((result, err) => {
		if(err){
			return false;
		}
		else{
			return true;
		};
	});
};

