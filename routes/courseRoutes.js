const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

	// For S39 Activity Code
	// Data needed in creation of course

	// contains all information needed in the function
	const data = {
		course: req.body, // contains name, description, price
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		courseController.addCourse(data.course).then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}

})

module.exports = router;